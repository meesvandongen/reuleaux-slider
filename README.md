# reuleaux-slider

<!-- [![npm package][npm-badge]][npm] -->

A React component of a three variable slider. 

## When to use  
If all variables can go up to one at the same time, this component could be useful. Look at the demo for an impression.

## Demo
[click here](https://meesvandongen.gitlab.io/reuleaux-slider/)

## Props

Prop | Type | args | Description
-- | -- | -- | --
knob           | node     |         |Component or element to use for the knob.
size           | number   |         |The width of the element. Excluding 10px margin.
backgroundColor| string   |         |The color of the triangle.
style          | object   |         |The style of the parent of the triangle.
isThreeWay     | bool     |         |A toggle between a two way and three way slider.
onChange       | function*|[A,B,C]  |Fires while the element is being changed.
value          | array    |         |The value of the slider components. [A,B,C] Should add up to one
labelA         | function |A        |The text on the label of angle A
labelB         | function |B        |The text on the label of angle B
labelC         | function |C        |The text on the label of angle C

> Note: "A" is left-bottom, "B" is right-bottom, "C" is (center-)top.

## Example
### Basic (uncontrolled)
```jsx
import Slider from 'reuleaux-slider';

render(
  <Slider
    onChange={e => console.log(e)}
  />, 
  document.querySelector('#root')
);
```

### Controlled
Currently not possible

[npm-badge]: https://img.shields.io/npm/v/reuleaux-slider.png?style=flat-square
[npm]: https://www.npmjs.org/package/reuleaux-slider

