import React, { Component } from 'react';
import t from 'prop-types';

const cos30 = 0.86602540378;
const heightFromTriangleWidth = size => size * cos30;
const lengthFromTrianglePoint = ({ x, y }) => (x * cos30) + (y * 0.5);


const round = (value, decimals) => Number(Math.round(value + 'e' + decimals) + 'e-' + decimals);

const clamp = (val, min, max) => {
  if (val < min) return min;
  if (val > max) return max;
  return val;
};

const widthForCurrentHeight = (maxWidth, currentHeight) =>
  clamp(maxWidth * (currentHeight / heightFromTriangleWidth(maxWidth)), 0, maxWidth);


const clampReuleaux = ({ x, y }, size) => {
  let tempX = x;
  let tempY = y;
  let relativeDistanceSquared = (tempX ** 2) + (((size * cos30) - tempY) ** 2); // A
  if (relativeDistanceSquared > size ** 2) {
    const divider = Math.sqrt(relativeDistanceSquared) / size;
    tempX /= divider;
    tempY = (size * cos30) - (((size * cos30) - tempY) / divider);
  }
  relativeDistanceSquared = (((size * cos30) - tempY) ** 2) + ((tempX - size) ** 2); // B
  if (relativeDistanceSquared > size ** 2) {
    const divider = Math.sqrt(relativeDistanceSquared) / size;
    tempX = size + ((tempX - size) / divider);
    tempY = (size * cos30) - (((size * cos30) - tempY) / divider);
  }
  relativeDistanceSquared = (tempY ** 2) + ((tempX - (size / 2)) ** 2); // C
  if (relativeDistanceSquared > size ** 2) {
    const divider = Math.sqrt(relativeDistanceSquared) / size;
    tempX = (size / 2) + ((tempX - (size / 2)) / divider);
    tempY /= divider;
  }
  return {
    x: tempX, y: tempY,
  };
};

const divideArrayByMax = (array) => {
  const max = Math.max(...array);
  return array.map(val => val / max);
};

const snapArray = array => array.map(number => round(number, 2));

export default class ReuleauxSlider extends Component {
  static propTypes = {
    knob: t.node,
    size: t.number,
    backgroundColor: t.string,
    style: t.object,
    isThreeWay: t.bool,
    onChange: t.func.isRequired,
    value: t.array,
    labelA: t.func,
    labelB: t.func,
    labelC: t.func,
  }
  static defaultProps = {
    knob: (
      <div style={{
        width: 20,
        height: 20,
        background: '#ccc',
        borderRadius: '50%',
      }}
      />),
    size: 400,
    backgroundColor: '#666',
    style: {},
    isThreeWay: true,
    value: undefined,
    labelA: val => val,
    labelB: val => val,
    labelC: val => val,
  }
  constructor() {
    super();
    this.slider = React.createRef();
  }
  state = {
    ratios: [
      0.33, 0.33, 0.33,
    ],
    clampedRelativeCoords: {
      x: 1, y: 1,
    },
  }

  componentDidMount = () => {
    this.bounds = this.slider.current.getBoundingClientRect();
  }
  componentDidUpdate = () => {
    this.bounds = this.slider.current.getBoundingClientRect();
  }

  onMouseDown = (e) => {
    this.onMouseMove(e);
    document.addEventListener('mouseup', this.onMouseUp);
    document.addEventListener('mousemove', this.onMouseMove);
    e.preventDefault();
  }
  onMouseUp = (e) => {
    document.removeEventListener('mouseup', this.onMouseUp);
    document.removeEventListener('mousemove', this.onMouseMove);
    e.preventDefault();
  }
  onMouseMove = (e) => {
    const relativeCoords = this.relativeCoords(e);

    const clampedRelativeCoords = clampReuleaux(relativeCoords, this.props.size);
    this.setState({ clampedRelativeCoords });

    const offset = 1;
    let ratios = [
      1 - clamp(
        ((1 + offset) * (Math.sqrt((((this.props.size * cos30) - clampedRelativeCoords.y) ** 2) + (clampedRelativeCoords.x ** 2)) / this.props.size)) - offset,
        0,
        1.0,
      ),
      1 - clamp(
        ((1 + offset) * (Math.sqrt((((this.props.size * cos30) - clampedRelativeCoords.y) ** 2) + ((clampedRelativeCoords.x - this.props.size) ** 2)) / this.props.size)) - offset,
        0,
        1.0,
      ),
      1 - clamp(
        ((1 + offset) * (Math.sqrt((clampedRelativeCoords.y ** 2) + ((clampedRelativeCoords.x - (this.props.size / 2)) ** 2)) / this.props.size)) - offset,
        0,
        1.0,
      ),
    ];
    ratios = divideArrayByMax(ratios);
    // ratios = snapArray(ratios);
    this.props.onChange(ratios);
    if (!this.isControlled()) {
      this.setState({
        ratios,
      });
    }
    this.setState({
      ratios,
    });

    e.preventDefault();
  }

  getCurrentRatios = () => {
    if (this.isControlled()) {
      return this.props.value;
    }
    return this.state.ratios;
  }

  ratiosFromCoords = ({ x, y }) => {
    const height = heightFromTriangleWidth(this.props.size);
    return [
      1 - (lengthFromTrianglePoint({
        x,
        y: -y + height,
      }) / height),
      1 - (lengthFromTrianglePoint({
        x: this.props.size - x,
        y: (y * -1) + height,
      }) / height),
      1 - (y / height),
    ];
  }

  coordsFromRatios = (ratios) => {
    const height = heightFromTriangleWidth(this.props.size);
    if (this.props.isThreeWay) {
      const y = height - ((1 - ratios[2]) * height);
      const x = (((1 - ratios[0]) * height) - (y * 0.5)) / cos30;
      return { x, y };
    }
    const x = (1 - ratios[0]) * this.props.size;
    const y = (1 - ratios[2]) * height;
    return { x, y };
  }

  isControlled = () => this.props.value !== undefined

  relativeCoords = (event) => {
    const { bounds } = this;
    const x = event.clientX - bounds.left;
    const y = event.clientY - bounds.top;
    return { x, y };
  }

  renderableCoords = () => {
    const currentCoords = this.coordsFromRatios(this.getCurrentRatios());
    if (this.props.isThreeWay) {
      return {
        x: currentCoords.x,
        y: ((currentCoords.y / heightFromTriangleWidth(this.props.size)) * 100) + '%',
      };
    }
    const horizontalSlice = widthForCurrentHeight(
      this.props.size,
      currentCoords.y,
    );
    const minCoordX = (this.props.size - horizontalSlice);
    return {
      x: (this.props.size / horizontalSlice) * (currentCoords.x - minCoordX),
      y: '50%',
    };
  }


  render() {
    const {
      size, style, knob, isThreeWay, backgroundColor,
    } = this.props;
    const renderableCoords = this.renderableCoords();
    return (
      <div
        style={{
          width: size,
          margin: 10,
        }}
      >
        <svg width="0" height="0">
          <clipPath id="svgPath" clipPathUnits="objectBoundingBox" >
            <path d="M.5 0a1 1 0 0 0-.5.866A.996.996 0 0 0 .5 1 .995.995 0 0 0 1 .866 1 1 0 0 0 .5 0z" />
            <path d="M.5 0L0 .866h1L.5 0z" />
          </clipPath>
        </svg>
        <div
          style={{
            textAlign: 'center',
            marginBottom: 10,
            display: isThreeWay ? 'block' : 'none',
          }}
        >
          {
            this.props.labelC((0.000001 + this.getCurrentRatios()[2]).toFixed(2))
            // 0.000001 prevents displaying negative number in some cases
          }
        </div>
        <div
          style={{
            position: 'relative',
            clipPath: 'url(#svgPath)',
            // clipPath: 'url(' + ReuleauxTriangle + '#svgPath)',
            overflow: 'hidden',
            ...style,
          }}
          role="grid"
          tabIndex={0}
          ref={this.slider}

          onMouseDown={this.onMouseDown}

        >
          <div style={{

            width: size,
            height: size,
            fontSize: 0,
            alignContent: 'bottom',
            transition: 'border 0.2s ease, width 0.2s ease',
            // background: 'url(' + ReuleauxTriangle + ')',
            background: '#333333',
          }}
          />
          <div
            style={{
              position: 'absolute',
              left: this.state.clampedRelativeCoords.x,
              bottom: this.props.size - this.state.clampedRelativeCoords.y,
              transform: 'translate(-50%, 50%)',
            }}
          >
            {knob}
          </div>
          <div style={{
            position: 'absolute',
            left: 200,
            top: 0,
            background: '#cc000066',
            width: (2 - this.state.ratios[2]) * this.props.size,
            height: (2 - this.state.ratios[2]) * this.props.size,
            borderRadius: '50%',
            transform: 'translate(-50%, -50%)',

          }}
          />
          <div style={{
            position: 'absolute',
            left: 0,
            top: cos30 * 400,
            background: '#cc000066',
            width: (2 - this.state.ratios[0]) * this.props.size,
            height: (2 - this.state.ratios[0]) * this.props.size,
            borderRadius: '50%',
            transform: 'translate(-50%, -50%)',

          }}
          />
          <div style={{
            position: 'absolute',
            left: 400,
            top: cos30 * 400,
            background: '#cc000066',
            width: (2 - this.state.ratios[1]) * this.props.size,
            height: (2 - this.state.ratios[1]) * this.props.size,
            borderRadius: '50%',
            transform: 'translate(-50%, -50%)',

          }}
          />
        </div>
        <div
          style={{
            marginTop: 10,
          }}
        >
          <span>
            {this.props.labelA((0.000001 + this.getCurrentRatios()[0]).toFixed(2))}
          </span>
          <span
            style={{ float: 'right' }}
          >
            {this.props.labelB((0.000001 + this.getCurrentRatios()[1]).toFixed(2))}
          </span>
        </div>
        <div>
          {
            (
              this.getCurrentRatios()
            ) ? 'isClamped' : '!isClamped'
          }
        </div>
      </div>
    );
  }
}
